//Подключаем стандартные библиотеки в стиле QML
//Основные концепции языка:
//Проект (Project), Продукт (Product), Артефакт (Artifact), Модуль (Module), Правило (Rule), Группа(Group), Зависимость (Depends), Тег (Tag).
//Продукт — это аналог pro или vcproj, т. е. одна цель для сборки.
//Проект — это набор ваших продуктов вместе с зависимостями, воспринимаемый системой сборки как одно целое. Один проект — один граф сборки.
//Тег — система классификации файлов. Например «*.cpp» => «cpp»
//Правило — Преобразование файлов проекта, отмеченных определенными тегами. Генерирует другие файлы, называемые Артефактами.
//Как правило, это компиляторы или другие системы сборки.
//Артефакт — файл, над который является выходным для правила (и возможно, входным для други правил). Это обычно «obj», «exe» файлы.
//У многих QML-объектов есть свойство condition, которое отвечает за то, будет собираться он или нет. А если нам необходимо разделить так файлы?
//Для этого их можно объединить в группу (Group)
//Rule умеет срабатывать на каждый файл, попадающий под что-то. Может срабатывать по разу на каждый фаил (например, для вызова компилятора), а может один раз на все (линкер).
//Transformer предназначен для срабатывания только на один фаил, с заранее определенным именем. Например, прошивальщик или какой-нибудь хитрый скрипт.
//флаг multiplex, который говорит о том, что это правило обрабатывает сразу все файлы данного типа скопом.

// We connect standard libraries in style QML
// Basic concepts of the language:
// Project, Product, Artifact, Module, Rule, Group, Depends, Tag.
// The product is an analog of pro or vcproj, that is, one target for the assembly.
// The project is a set of your products together with dependencies, perceived by the build system as one. One project - one assembly graph.
// Tag - file classification system. For example, "* .cpp" => "cpp"
// Rule - Converting project files marked with certain tags. Generates other files called Artifacts.
// Typically, these are compilers or other build systems.
// Artifact is the file over which is the output for the rule (and possibly the input for other rules). These are usually "obj", "exe" files.
// Many QML objects have a condition property, which is responsible for whether it will be assembled or not. And if we need to split the files like this?
// To do this, you can group them into a group (Group)
// Rule is able to work on every file that falls under something. It can trigger once per each file (for example, to call the compiler), but can once on all (linker).
// Transformer is intended for triggering only one file, with a predefined name. For example, the broacher or some cunning script.
// the multiplex flag, which says that this rule processes all files of this type at once.

import qbs
import qbs.FileInfo
import qbs.ModUtils

CppApplication {

    // основной элемент файла - проект.

    //    moduleSearchPaths: "qbs" // Папка для поиска дополнительных модулей, таких как cpp и qt

    // Один проект может состоять из нескольких продуктов - конечных целей сборки.
    // Один проект может состоять из нескольких продуктов - конечных целей сборки.
    // указываем связанные файлы с помощью references. Внимание: это не жестко заданный порядок!
    // Порядок задается с помощью зависимостей, о них позже
    //    references: [
    //           "*.qbs",
    //       ]

    // the main element of the file is the project.

    // moduleSearchPaths: "qbs" // Folder for finding additional modules, such as cpp and qt

    // One project can consist of several products - the final assembly goals.
    // One project can consist of several products - the final assembly goals.
    // specify the related files with references. Attention: this is not a rigidly prescribed order!
    // The order is specified using dependencies, about them later
    // references: [
    // "* .qbs",
    //]


    name: "qbs-gd32f103"
    // Название выходного файла (без суффикса, он зависит от цели)
    // The name of the output file (without the suffix, it depends on the purpose)
    type: [
        "application",
        "bin",
        "hex",
        // Тип - приложение, т.е. исполняемый файл.
        // Type - application, i.e. executable file.
    ]

    Depends {
        name: "cpp"
        // Этот продукт зависит от компилятора C++
        // This product depends on the C ++ compiler
    }

    consoleApplication: true
    cpp.positionIndependentCode: false
    cpp.executableSuffix: ".elf"    

    property string home_path: path + "/.."
    property string linker_scripts_path: home_path + "/ldscripts"
    property string libs_path: home_path + "/libs"
    property string cmsis_path: libs_path + "/cmsis"
    property string periphery_path: libs_path + "/periphery"
    property string port_path: libs_path + "/port/gcc"
    property string system_path: libs_path + "/system"


    property string app_path: home_path + "/src/app"
/*
    Group {
        // Имя группы
        // A group name
        name: "Template"
        // Список файлов в данном проекте.
        // List of files in this project.
        files: [
        ]
        // Каталоги с включенными файлами
        // Directories with included files
        cpp.includePaths: [
        ]
        // Пути до библиотек
        // Paths to Libraries
        cpp.libraryPaths: []
    }

*/

    Group {
        name: "libs"
        files: [
            libs_path + "/**/*.h",
            libs_path + "/**/*.c",
            libs_path + "/**/*.s",
        ]
        excludeFiles: [
        ]
/*        cpp.cxxFlags: [ "-std=c++11" ]
        cpp.cFlags: [ "-std=gnu99" ]
        cpp.warningLevel: "all"*/
    }

    Group {
        name: "app"
        files: [
            app_path + "/*",
        ]
    }



    Group {
        // Имя группы
        // A group name
        name: "linkersrcipts"
        // Список файлов в данном проекте
        // List of files in this project
        files: [
            linker_scripts_path + "/*.ld",
            linker_scripts_path + "/*.lds",
        ]
    }

    // Каталоги с включенными файлами
    // Directories with included files
    cpp.includePaths: [
        cmsis_path,
        periphery_path,
        port_path,
        system_path,
        app_path,
    ]

    cpp.assemblerFlags: [
        "-march=rv32imac",
        "-mabi=ilp32",
        "-mcmodel=medlow",
    ]

    cpp.defines: [
        "USE_STDPERIPH_DRIVER",
        "HXTAL_VALUE=25000000",
    ]

    cpp.commonCompilerFlags: [
        "-march=rv32imac",
        "-mabi=ilp32",
        "-mcmodel=medlow",
    ]

    cpp.driverFlags: [
        "-march=rv32imac",
        "-mabi=ilp32",
        "-mcmodel=medlow",
        "-Xlinker",
        "--gc-sections",
        "-specs=nosys.specs",
        "-specs=nano.specs",

    ]

    cpp.linkerFlags: [
        "--start-group",
        "-T" + linker_scripts_path + "/GD32VF103x8.lds",
        "--end-group",
    ]

    cpp.libraryPaths: [

    ]

    cpp.staticLibraries: [

    ]

    Properties {
        condition: qbs.buildVariant === "debug"
        cpp.debugInformation: true
        cpp.optimization: "none"
    }

    Properties {
        condition: qbs.buildVariant === "release"
        cpp.debugInformation: false
        cpp.optimization: "small"
        // Виды оптимизаций
        // Types of optimizations
        // "none", "fast", "small"
    }

    Properties {
        condition: cpp.debugInformation
        cpp.defines: outer.concat("DEBUG")
    }

    Group {
        // Properties for the produced executable
        // Свойства созданного исполняемого файла
        fileTagsFilter: product.type
        qbs.install: true
    }

    // Создать .bin файл
    // Create a .bin file
    Rule {
        id: binDebugFrmw
        condition: qbs.buildVariant === "debug"
        inputs: ["application"]

        Artifact {
            fileTags: ["bin"]
            filePath: input.baseDir + "/" + input.baseName + ".bin"
        }

        prepare: {
            var objCopyPath = "riscv64-elf-objcopy"
            var argsConv = ["-O", "binary", input.filePath, output.filePath]
            var cmd = new Command(objCopyPath, argsConv)
            cmd.description = "Converting to BIN: " + FileInfo.fileName(
                        input.filePath) + " -> " + input.baseName + ".bin"

            // Запись во внутреннюю память
            // Write to the internal memory
            var argsFlashingInternalFlash =
            [           "-f", project.sourceDirectory + "/../openocd/openocd_ub.cfg",
                        "-c", "init",
                        "-c", "reset init",
                        "-c", "flash write_image erase " + input.filePath,
                        "-c", "reset",
                        "-c", "shutdown"
            ]

            var cmdFlashInternalFlash = new Command("openocd", argsFlashingInternalFlash);
            cmdFlashInternalFlash.description = "Wrtie to the internal flash"

            return [cmd, cmdFlashInternalFlash]
            //return [cmd, cmdFlashQspi, cmdFlashInternalFlash]
        }
    }

    // Создать .bin файл
    // Create a .bin file
    Rule {
        id: binFrmw
        condition: qbs.buildVariant === "release"
        inputs: ["application"]

        Artifact {
            fileTags: ["bin"]
            filePath: input.baseDir + "/" + input.baseName + ".bin"
        }

        prepare: {
            var objCopyPath = "riscv64-elf-objcopy"
            var argsConv = ["-O", "binary", input.filePath, output.filePath]
            var cmd = new Command(objCopyPath, argsConv)
            cmd.description = "converting to BIN: " + FileInfo.fileName(
                        input.filePath) + " -> " + input.baseName + ".bin"

            // Запись во внутреннюю память
            // Write to the internal memory
            var argsFlashingInternalFlash =
            [           "-f", project.sourceDirectory + "/../openocd/openocd_ub.cfg",
                        "-c", "init",
                        "-c", "reset init",
                        "-c", "flash write_image erase " + input.filePath,
                        "-c", "reset",
                        "-c", "shutdown"
            ]

            var cmdFlashInternalFlash = new Command("openocd", argsFlashingInternalFlash);
            cmdFlashInternalFlash.description = "Wrtie to the internal flash"

            return [cmd, cmdFlashInternalFlash]
            //return [cmd, cmdFlashQspi, cmdFlashInternalFlash]
        }
    }

    // Создать .hex файл
    // Create a .hex file
    Rule {
        id: hexFrmw
        condition: qbs.buildVariant === "release"
        inputs: ["application"]

        Artifact {
            fileTags: ["hex"]
            filePath: input.baseDir + "/" + input.baseName + ".hex"
        }

        prepare: {
            var objCopyPath = "riscv64-elf-objcopy"
            var argsConv = ["-O", "ihex", input.filePath, output.filePath]
            var cmd = new Command(objCopyPath, argsConv)
            cmd.description = "converting to HEX: " + FileInfo.fileName(
                        input.filePath) + " -> " + input.baseName + ".hex"

            return [cmd]
        }
    }
}
